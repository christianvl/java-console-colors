# Java Console Colors

<p>This is a simple Java class to print colored text to the console.

## Instructions 

<p>You can simply import this class and use any of the static strings with the color code to apply to a text string.

<p>To use it, create a ConsoleColors object parameterized with a text color, a background color, and the text String to apply the styles to (invalid codes will result in no style). Ex.:

```Java
ConsoleColors colors = new ConsoleColors(ConsoleColors.TEXT_RED, ConsoleColors.TEXT_BG_GREEN, "text string");
System.out.println (colors.getColoredString());
```
<p>There are setters and getters to change or get each parameter of the object (text color, background color, text string without the colors, full string with the color codes.

<p>The colored string also contains the ANSI code at the end to reset all the styles (so there should not be any interference to further console output). 

## License
<p> This software is licensed under the [LGPL3](https://www.gnu.org/licenses/lgpl-3.0.html) license.
<p> ![LGPL](https://www.gnu.org/graphics/lgplv3-88x31.png "LGPL3") 
