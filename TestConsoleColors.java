
/** This is a test class */

import javaconsolecolors.ConsoleColors;

public class TestConsoleColors{

    public static void main(String args[]){

        String text = "This is a test";

        System.out.println(text);

        ConsoleColors colors = new ConsoleColors(ConsoleColors.TEXT_RED, ConsoleColors.TEXT_BG_GREEN, text);

        System.out.println(colors.getColoredString());

        colors.setTextColor(colors.TEXT_YELLOW);
        colors.setBgColor(colors.TEXT_BG_RED);

        System.out.println(text);

        System.out.println(colors.getColoredString());

        colors.setTextString(null);

        System.out.println(colors.getColoredString());

    }

}
